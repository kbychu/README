## 2022-07-05

- Recovery and catch-up
- 15.3 planning

## 2022-06-27

- COVID 😱

## 2022-06-13
- Short week for Kevin (first days of summer break for kids) and Nicole (out sick)
- Completed [15.2 planning](https://gitlab.com/gitlab-org/monitor/respond/-/issues/105)
- Realized that we need to do something about [alerts](https://gitlab.com/groups/gitlab-org/-/epics/8257)
- Good [discussion](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15890) on Dogfooding Incident Timeline
- Learned about the manage team completing DORA metrics for MTTR - we need to figure out how to have the data from incident timeline serve as the source of truth OOTB

## 2022-06-07
- Participant [screener](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51hwooyUzybfbOC) has been passed to UX Research to begin recruiting
- Created a [draft feature issue](https://gitlab.com/gitlab-org/gitlab/-/issues/364491) for proposed Slackbot integration for Respond 
- Created [an issue to deprecate all Zoom-related metrics](https://gitlab.com/gitlab-org/product-intelligence/-/issues/588#note_985960769) from service ping 
- [15.2 planning](https://gitlab.com/gitlab-org/monitor/respond/-/issues/105)
- [Dogfood incident timeline activities](https://gitlab.com/gitlab-org/gitlab/-/issues/360452)
- Completed solution validation for [Incident Tag](https://gitlab.com/gitlab-org/gitlab/-/issues/336026)

## 2022-06-03
- Created [screener](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51hwooyUzybfbOC) for Incident Review Problem Validation 
- Continuing to make progress on [the overhaul of the Respond Group Dashboard](https://gitlab.com/groups/gitlab-data/-/epics/557)

## 2022-05-26

- Short week - returned from Kubecon EU
- Amelia is doing solution validation on [incident timeline timestamps](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1911)
- Updated [Incident Management strategy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105049)
- Added [goal for end of FY24](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104826)
- Nicole is [recruiting for Incident Review Problem Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1951)
- [On-call schedule management direction added](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105110)

## 2022-05-13

- Edited [Problem Validation for Incident Retrospectives](https://gitlab.com/gitlab-org/ux-research/-/issues/1925) 
- Identified nuances between [SaaS and Self-Managed for Respond](https://gitlab.com/gitlab-data/analytics/-/issues/12718#note_943633840) metrics
- Created issues for creating an [Operations Visibility Toggle](https://gitlab.com/groups/gitlab-org/-/epics/8004)
- Created [discussion guide](https://docs.google.com/document/d/1nn6CyHR4POJNqvi6gJ4S-feCUIP-0xLnNmRkEzEaXyU/edit#heading=h.368vse1ddcij) for Problem Validation for Incident Retrospectives  

## 2022-05-06

- [UX Roadmap Workshop](https://gitlab.com/gitlab-org/gitlab/-/issues/360804)
- [15.1 plan](https://gitlab.com/gitlab-org/monitor/respond/-/issues/104) and [kickoff](https://gitlab.slack.com/archives/C02SHPPGZS5/p1651856919815669)
- Making progress on problem and solution clarity for [incident timestamps](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1911)
- Completed Blameless Competitive Analysis](https://gitlab.com/gitlab-org/monitor/respond/-/issues/119)
- Connected with integration group on [Slack Integration](https://gitlab.com/groups/gitlab-org/-/epics/7536)

## 2022-04-29

- Thinking a lot about [runbooks](https://gitlab.com/gitlab-org/gitlab/-/issues/360714), [Incident roles](https://gitlab.com/gitlab-org/gitlab/-/issues/360957), and Slack integration
- Working with Amelia and Sarah on capturing [incident start and end time](https://gitlab.com/gitlab-org/gitlab/-/issues/336026)
- Started working on [15.1 planning](https://gitlab.com/gitlab-org/monitor/respond/-/issues/104)
- Completed [FireHydrant Competitive Analysis](https://gitlab.com/gitlab-org/monitor/respond/-/issues/117)
- Started [problem validation research](https://gitlab.com/gitlab-org/ux-research/-/issues/1925) for post-incident retrospectives 

## 2022-04-22

- RPIs for 14.10
- [Updated vision & overview for incident management](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102951)
- Working with Amelia to update the [roadmap](https://gitlab.com/groups/gitlab-org/-/epics/7013)

## 2022-04-15

- Added opportunity trees to the stages of the [Incident Response Workflow](https://app.mural.co/t/gitlab2474/m/gitlab2474/1648752385561/ef9dc7f17d476526f19051061db46ac9086e8092?sender=ua9be69f6d8a08dffbbd64048)
- Milestone Planning for 15.0 
- Added Roles to Be Engaged Based on Severity Level to the [Incident Role mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1648753644760/84b3ac1ce34832b4e9094f99b9fc989bf2e6ff3f?sender=ua9be69f6d8a08dffbbd64048) 
- Discussion of capacity of engineering team for 15.0 
- Update roadmap with Amelia

## 2022-04-08 

- Reviewed [incident response workflow](https://app.mural.co/t/gitlab2474/m/gitlab2474/1648752385561/ef9dc7f17d476526f19051061db46ac9086e8092?sender=ua9be69f6d8a08dffbbd64048) with Incident Management Working Group 
- Created [incident role definitions](https://app.mural.co/t/gitlab2474/m/gitlab2474/1648753644760/84b3ac1ce34832b4e9094f99b9fc989bf2e6ff3f?sender=ua9be69f6d8a08dffbbd64048) in Mural - additional deliverables planned out by the group 
- Creating RPI for the Incident Timeline MVC 
- Adjusting documentation in anticipation of the 14.10 release post 

## 2022-04-01

- Updated docs to reflect removal of [metrics and self-monitoring will be 16.0](https://gitlab.com/gitlab-org/gitlab/-/issues/356728)
- Adjust 14.10 plan accordingly based on above
- Start documenting overall incident management workflow in mural
- Nicole G internship started!
- [Update Continuous Verification JTBD](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101730)
