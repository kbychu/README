I learn best by doing, followed closely by reading and writing.

I can become the most effective version of myself by continuing to improve. By doing. By reading. By writing. This page is a list of things I want to accomplish that is outside of the direct scope of my job. I intend to update this page several times per year, ideally monthly.

## Things to do
* Figure out using GitPod for GitLab
* ~~Complete [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)~~
* ~~Complete [Prometheus course](https://www.udemy.com/course/monitoring-and-alerting-with-prometheus/)~~
* Build something! Ideas welcome
  * Started building [Bongo Tanuki](https://bongo-tanuki.surge.sh/)!
* Learn Vue
* Contribute to something in Ruby and Javascript to GitLab.
  * I'd like contribute to making the contribution chart scrollable

## Things to read
* ~~[Continuous Delivery](https://www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912)~~
* [Unicorn project](https://www.amazon.com/Unicorn-Project-Developers-Disruption-Thriving-ebook/dp/B07QT9QR41)
* [Terraform UP & Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code/dp/1492046906)
* ~~[A Man called Ove](https://www.amazon.com/Man-Called-Ove-Novel-ebook/dp/B00GEEB730)~~
* ~~[A Thousand Splendid Suns](https://www.amazon.com/Thousand-Splendid-Suns-Khaled-Hosseini-ebook/dp/B000SCHC0Q)~~
* [Sapiens A Brief History of Humankind](https://www.amazon.com/Sapiens-Humankind-Yuval-Noah-Harari-ebook/dp/B00ICN066A)
* ~~[First Round Essentials - Management](https://books.firstround.com/management/)~~
* ~~[The Making of a Manager](https://www.amazon.com/Making-Manager-What-Everyone-Looks/dp/0735219567)~~
* [Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)

In addition, I will start adding articles/blog posts etc to [my Pocket list](https://getpocket.com/@28fd7p24T3657g2086A54f3A13g5T2e6eDbIf9c0g6d9c6xKoWomvHjtzfvKr3aN)

## Things to write (in general a proposed title for a blog post)
* Universally applicable lessons from devops
* Managing product managers remotely
* The advantages of being a remote product manager
* How monitoring is changing
* Releasing software: the more things change the more they stay the same
